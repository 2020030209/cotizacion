function cotizar(){
    if(document.getElementById("valor").value > 0 && document.getElementById("planes").value != ""){ 

        let txtValor = document.getElementById("valor").value;   
        let mes = document.getElementById("planes").value;
        let txtFinanciar;
        let interes;
        let num;
        switch(mes){
            case "12":
                interes = 12.5;
                num = 12;
                break;

            case "18":
                interes = 17.2;
                num = 18;
                break;

            case "24":
                interes = 21;
                num = 24;
                break;

            case "36":
                interes = 26;
                num = 36;
                break;

            case "48":
                interes = 45;
                num = 48
                break;
        } 
        let txtEnganche = txtValor *.30;
        document.getElementById("enganche").value = txtEnganche; 

        txtFinanciar = ((txtValor - txtEnganche) + ((txtValor - txtEnganche) * interes/100));
        document.getElementById("total").value = txtFinanciar;
        let pagoMensual = txtFinanciar/num;
        document.getElementById("pago").value = pagoMensual;
    }else{
        alert("Opcion no valida")
    }
    
}

function limpiar(){
    document.getElementById("valor").value = "";
    document.getElementById("enganche").value = "";
    document.getElementById("planes").value = "";
    document.getElementById("total").value = "";
    document.getElementById("pago").value = "";
}